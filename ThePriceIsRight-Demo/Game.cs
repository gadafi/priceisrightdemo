﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThePriceIsRight_Demo
{
    enum GuessResult
    {
        TooHigh,TooLow, Equal
    }


    class Game
    {
        //this should be a random number
        private int _productPrice;


        public GuessResult Check(int price)
        {
            if (price < _productPrice)
                return GuessResult.TooLow;
            else if (price > _productPrice)
                return GuessResult.TooHigh;
            else
                return GuessResult.Equal;


        }

        public void Start()
        {
            //generate a random number
            Random random = new Random();

            _productPrice = random.Next(0, 10);

        }
    }
}
