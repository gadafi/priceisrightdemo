﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ThePriceIsRight_Demo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private Game _game = new Game();

        public MainPage()
        {
            this.InitializeComponent();

            _game.Start();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int userGuess = int.Parse(UserInput.Text);
            //todo: verify the user's input 

            
            // this is not good because ...............
            //Game game = new Game();
            //game.Start();

            if (_game.Check(userGuess) == GuessResult.TooHigh)
            {
                OutputResult.Text = "Too high!";
            }
            else if (_game.Check(userGuess) == GuessResult.TooLow)
            {
                OutputResult.Text = "Too low!";
            }
            else
            {
                OutputResult.Text = "You got it!";
                MessageDialog dialog = new MessageDialog("A new game has started!");
                dialog.ShowAsync();
                _game.Start();
            }
        }

        private void UserInput_TextChanged(object sender, TextChangedEventArgs e)
        {

            OutputResult.Text = "";
        }
    }
}
